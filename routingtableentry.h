#ifndef __ROUTINGTABLEENTRY_H__
#define __ROUTINGTABLEENTRY_H__

#include <string>
#include <tins/ip_address.h>
#include <QMetaType>

using namespace Tins;
using std::string;

struct RoutingTableEntry
{
    uint8_t ad = 0;
    int metric = 0;
    string network = string();
    string mask = string();
    string nextHop = string();
    string outIface = string();
};

Q_DECLARE_METATYPE(RoutingTableEntry)

#endif // __ROUTINGTABLEENTRY_H__
