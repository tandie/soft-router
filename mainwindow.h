#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__

#include <QMainWindow>
#include <QMetaType>
#include "packetanalyzer.h"
#include "arptable.h"
#include "routingtable.h"
#include "staticroutedialog.h"
#include "arptimerdialog.h"
#include "ipaddressdialog.h"
#include "riptimerdialog.h"
#include "ripdatabase.h"

struct PacketInfo;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void showRoute(const RoutingTableEntry &entry);
    void hideRoute(int pos);
    void showArpEntry(const ArpEntry &entry);
    void hideArpEntry(int pos);
    void resetArpEntry(int pos);
    void updateArpEntry(int pos, const PacketInfo &pktInfo);
    void updateIPParams(int port, QString ipAddr, QString mask);
    void updateArpTimer(QString time);
    void toggleRip(int portNum, int port);

signals:
    void clearArpEntries();

private:
    void initialiseTable();
    void printAllInterfaces();
    void addToArpTable(const PacketInfo &packet);
    QString getPort(int port) const;
    void snifferRun();
    void snifferAbort();
    void showEvent(QShowEvent *event) override;
    void closeEvent(QCloseEvent *event) override;

    Ui::MainWindow *ui;
    StaticRouteDialog staticRouteDialog;
    ArpTimerDialog arpTimerDialog;
    IPAddressDialog ipAddressDialog;
    RipTimerDialog ripTimerDialog;
    ArpTable arpTable;
    RoutingTable routingTable;
    PacketAnalyzer sniffers[2];
    RipDatabase ripDatabase;
    uint8_t updateTimer;
    bool sniffOnRun;

private slots:
    void handleError(const char *err_msg);
    void updateArpEntryTimer();
    void sendRIPResponse();

    void on_actionStatick_cestu_triggered();
    void on_actionARP_asova_triggered();
    void on_actionIP_adresy_triggered();
    void on_actionRIP_asova_e_triggered();
    void on_btnClearArp_clicked();
    void on_p2RipEnable_stateChanged(int arg1);
    void on_p1RipEnable_stateChanged(int arg1);

    void on_action_tart_triggered();
    void on_actionStop_triggered();
};

#endif // __MAINWINDOW_H__
