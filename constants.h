#ifndef CONSTANTS_H
#define CONSTANTS_H

enum Constants
{
    REQUEST = 0x01,
    RESPONSE = 0x02,
    VERSION = 0x02,
    MUST_BE_ZERO_8 = 0x00,
    ROUTE_TAG_8 = 0x00,
    AFI_0 = 0x02,
    AFI_1 = 0x00,
    METRIC_8 = 0x00,
    METRIC_32 = 0x00000000,
    EMPTY_OCTET = 0x00,
    EMPTY_IP = 0x00000000,
    FULL_OCTET = 0xFF,
    UNREACHABLE = 0x0F,
    CONNECTED_AD = 0,
    STATIC_AD = 1,
    RIP_AD = 120,
    RIP_PORT = 520
};

#endif // CONSTANTS_H
