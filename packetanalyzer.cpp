#include "packetanalyzer.h"
#include <sstream>
#include <unistd.h>

using namespace Tins;
using std::cout;
using std::endl;
using std::string;
using std::istringstream;
using SoftRouter::InterfaceDown;

PacketAnalyzer::PacketAnalyzer()
{
    connect(&this->worker, SIGNAL(started()), this, SLOT(run()));
    ripEnable = false;
}

void PacketAnalyzer::abortSniffing()
{
    this->abort.store(true);
    this->worker.exit();
    this->worker.wait();
}

void PacketAnalyzer::startSniff(const string &name)
{
    try {
        portName = name;
        this->port = new NetworkInterface(portName);
    }
    catch (Tins::invalid_interface &)
    {
        throw InterfaceDown("Opa, zabudli ste pripojiť rozhranie.");
        return;
    }

    this->sender = new PacketSender(*port);

    if (!this->port->is_up())
    {
        throw InterfaceDown("Opa, skúste odpojiť a pripojiť rozhranie.");
        return;
    }

    this->abort = false;
    this->moveToThread(&this->worker);
    this->worker.start();
}

void PacketAnalyzer::run()
{
    try {
        SnifferConfiguration conf;
        conf.set_promisc_mode(true);
        conf.set_immediate_mode(true);
        conf.set_direction(PCAP_D_IN);
        Sniffer sniffer(portName, conf);
        sniffer.sniff_loop(std::bind(&PacketAnalyzer::handle, this, std::placeholders::_1));
    }
    catch (socket_open_error &)
    {
        emit raise("Spustite aplikáciu s administrátorskými právami.");
        return;
    }
    catch (...)
    {
        emit raise("Vyskytla sa neočakávaná chyba. Skúste znova.");
        return;
    }

    return;
}

bool PacketAnalyzer::handle(const Packet &packet)
{
    PacketInfo pktInfo;

    handleARP(packet, pktInfo);
    handleIP(packet, pktInfo);

    return !this->abort.load();
}

void PacketAnalyzer::handleARP(const Packet &packet, PacketInfo &pktInfo)
{
    const ARP *arp = packet.pdu()->find_pdu<ARP>();
    const EthernetII *ether = packet.pdu()->find_pdu<EthernetII>();

    if (arp == NULL || ether == NULL)
        return;

    pktInfo.srcProtAdd = arp->sender_ip_addr().to_string();
    pktInfo.dstProtAdd = arp->target_ip_addr().to_string();
    pktInfo.srcHwAdd = arp->sender_hw_addr().to_string();
    pktInfo.dstHwAdd = ether->dst_addr().to_string();
    uint16_t oper = arp->opcode();

    if (oper == ARP::REQUEST)
    {
        QString protAddr = QString::fromStdString(arp->target_ip_addr().to_string());
        ArpEntry entry = findDstMac(protAddr);

        if (!entry.hwAddr.empty() || isKnownNetwork(protAddr))
        {
            cout << endl;
            cout << "ARP: VYTVARAM ARP ODPOVED" << endl;
            EthernetII arpReply = ARP::make_arp_reply(
                        arp->sender_ip_addr(),
                        arp->target_ip_addr(),
                        arp->sender_hw_addr(),
                        macAddr);
            cout << "ARP: " << arp->target_ip_addr().to_string() << " -> " << macAddr.to_string() << endl;

            try
            {
                sender->send(arpReply);
            }
            catch (invalid_interface &)
            {
                emit raise("Nastala chyba pri odosielaní paketu");
                return;
            }
            emit arpPacketReceived(pktInfo);
        }
    }
    else if (oper == ARP::REPLY)
    {
        emit arpPacketReceived(pktInfo);
    }
}

void PacketAnalyzer::handleIP(const Packet &packet, PacketInfo &pktInfo)
{
    const IP *ip = packet.pdu()->find_pdu<IP>();
    const EthernetII *ether = packet.pdu()->find_pdu<EthernetII>();

    if (ip == NULL || ether == NULL)
        return;

    const UDP *udp = packet.pdu()->find_pdu<UDP>();
    const ICMP *icmp = packet.pdu()->find_pdu<ICMP>();

    pktInfo.dstProtAdd = ip->dst_addr().to_string();
    pktInfo.srcProtAdd = ip->src_addr().to_string();
    pktInfo.srcHwAdd = ether->src_addr().to_string();
    pktInfo.dstHwAdd = ether->dst_addr().to_string();

    if (udp != NULL && udp->dport())
    {
        if (!ipAddr.empty() && !mask.empty())
        {
            IPv4Range range = IPv4Range::from_mask(ipAddr, mask);
            if (range.contains(ip->src_addr()))
                handleRIP(packet, pktInfo);
        }
        return;
    }

    if (icmp != NULL &&
        (ip->dst_addr() == ipAddr ||
         ip->dst_addr() == neighAddr))
    {
        handleICMP(packet, pktInfo);
        return;
    }

    if (ip->dst_addr().is_broadcast() || ip->dst_addr().is_multicast())
        return;

    QString dstProtAddr = QString::fromStdString(pktInfo.dstProtAdd);
    RoutingTableEntry routingEntry = lookupRoute(dstProtAddr);

    if (!routingEntry.outIface.empty())
    {
        cout << "POSIELAM PAKET NA ROZHRANIE " << routingEntry.outIface << endl;

        QString dstMac = QString::fromStdString(pktInfo.dstProtAdd);
        ArpEntry arpEntry = findDstMac(dstMac);

        cout << "ARP: CIELOVA MAC ADRESA JE " << arpEntry.hwAddr << endl;

        if (!arpEntry.hwAddr.empty())
        {
            if (routingEntry.outIface == portName)
                forwardPacket(arpEntry.hwAddr, *ip);
            else
               neighborForwardPacket(arpEntry.hwAddr, *ip);
        }
        else
        {
            if (routingEntry.outIface == portName)
                ARPRequest(pktInfo);
            else if (routingEntry.outIface == neighName)
                neighborARPRequest(pktInfo);
        }
    }
    return;
}

void PacketAnalyzer::handleICMP(const Packet &packet, PacketInfo &pktInfo)
{
    const ICMP *icmp = packet.pdu()->find_pdu<ICMP>();
    const IP *ip = packet.pdu()->find_pdu<IP>();
    const EthernetII *ether = packet.pdu()->find_pdu<EthernetII>();

    const ICMP::Flags type = icmp->type();

    if (pktInfo.dstProtAdd == ipAddr ||
        pktInfo.dstProtAdd == neighAddr)
    {
        if (type == ICMP::ECHO_REQUEST)
        {
            QString dstIPAddr = QString::fromStdString(pktInfo.dstProtAdd);
            ArpEntry arpEntry = findDstMac(dstIPAddr);

            if (arpEntry.hwAddr.empty())
                ARPRequest(pktInfo);

            ICMP *icmp_pkt = icmp->clone();
            icmp_pkt->set_echo_reply(icmp->id(), icmp->sequence());
            EthernetII frame = EthernetII(ether->src_addr(), port->hw_address()) /
                               IP(ip->src_addr(), ipAddr) /
                                *icmp_pkt;
            try
            {
                sender->send(frame);
            }
            catch (invalid_interface &)
            {
                emit raise("Nastala chyba pri odosielaní paketu");
                return;
            }
        }
        else if (type == ICMP::ECHO_REPLY)
            return;

    }
}

void PacketAnalyzer::handleRIP(const Packet &packet, PacketInfo &pktInfo)
{
    if (!ripEnable)
        return;

    const UDP *udp = packet.pdu()->find_pdu<UDP>();
    const IP *ip = packet.pdu()->find_pdu<IP>();
    const EthernetII *ether = packet.pdu()->find_pdu<EthernetII>();

    if (udp == NULL || ip == NULL || ether == NULL)
        return;

    if (udp->dport() == Constants::RIP_PORT)
    {
        const RawPDU *raw = packet.pdu()->find_pdu<RawPDU>();

        if (raw == NULL)
            return;

        vector<int> payload(raw->payload().begin(), raw->payload().end());
        int command = payload.at(0);

        if (command == 2)
        {
            std::ostringstream networkAddress;
            std::ostringstream mask;
            RipDatabaseEntry entry;

            for (uint32_t i = 0, idx = 8; idx < raw->size(); i++, idx += 20)
            {
                networkAddress << payload.at(idx) << "."
                               << payload.at(idx+1) << "."
                               << payload.at(idx+2) << "."
                               << payload.at(idx+3);

                mask << payload.at(idx+4) << "."
                     << payload.at(idx+5) << "."
                     << payload.at(idx+6) << "."
                     << payload.at(idx+7);

                cout << "RIP -> SIET: " << networkAddress.str() << endl;
                cout << "RIP -> MASKA: " << mask.str() << endl;
                cout << "RIP -> METRIKA: " << payload.at(idx+15) << endl;

                try
                {
                    entry.network = networkAddress.str();
                    entry.mask = mask.str();
                    entry.nextHop = pktInfo.srcProtAdd;
                    entry.metric = payload.at(idx+15);
                    entry.sourcePort = portName;
                    emit sendToRipDatabase(entry);
                }
                catch (...)
                {
                    emit raise("Paket nebol korektne spracovaný");
                    return;
                }

                networkAddress.str("");
                mask.str("");
            }
        }
        else if (command == 1)
        {
            cout << "Spracuvam RIP Request" << endl;
        }
    }
}

void PacketAnalyzer::sendRip()
{
    if (!ripEnable)
        return;

    vector<uint8_t> payload;
    char dot;
    int bytes[4];
    int j = 0;
    istringstream input;

    payload.push_back(Constants::RESPONSE);
    payload.push_back(Constants::VERSION);
    payload.push_back(Constants::MUST_BE_ZERO_8);
    payload.push_back(Constants::MUST_BE_ZERO_8);

    QString iface = QString::fromStdString(portName);

    vector<RipDatabaseEntry> routes = getRipRoutes(iface);

    for (RipDatabaseEntry route : routes)
    {
        payload.push_back(Constants::AFI_1);
        payload.push_back(Constants::AFI_0);
        payload.push_back(Constants::ROUTE_TAG_8);
        payload.push_back(Constants::ROUTE_TAG_8);

        input = istringstream(route.network);
        input >> bytes[0] >> dot >> bytes[1] >> dot >> bytes[2] >> dot >> bytes[3] >> dot;

        for (j = 0; j < 4; j++)
            payload.push_back(bytes[j]);

        input = istringstream(route.mask);
        input >> bytes[0] >> dot >> bytes[1] >> dot >> bytes[2] >> dot >> bytes[3] >> dot;

        for (j = 0; j < 4; j++)
            payload.push_back(bytes[j]);

        for (j = 0; j < 4; j++)
            payload.push_back(0);

        for (j = 0; j < 3; j++)
            payload.push_back(0);

        payload.push_back(route.metric);
    }

    EthernetII eth =
            EthernetII("01:00:5e:00:00:09", port->hw_address()) /
            IP("224.0.0.9", ipAddr) /
            UDP(520, 520) /
            RawPDU(payload);

    try
    {
        sender->send(eth);
    }
    catch (invalid_interface &)
    {
        emit raise("Nastala chyba pri odosielaní paketu");
        return;
    }
    catch (socket_write_error)
    {
        emit raise("Opa, sprava je prilis dlha");
        return;
    }
}

void PacketAnalyzer::ARPRequest(PacketInfo &pktInfo)
{
    EthernetII ARPRequest =
            ARP::make_arp_request(
                pktInfo.dstProtAdd,
                ipAddr,
                macAddr);
    try
    {
        sender->send(ARPRequest);
    }
    catch (invalid_interface &)
    {
        emit raise("Nastala chyba pri odosielaní paketu");
        return;
    }
}

void PacketAnalyzer::neighborARPRequest(PacketInfo &pktInfo)
{
    EthernetII ARPRequest =
            ARP::make_arp_request(
                pktInfo.dstProtAdd,
                neighAddr,
                neighMacAddr);

    try
    {
        neighborSender->send(ARPRequest);
    }
    catch (invalid_interface &)
    {
        emit raise("Nastala chyba pri odosielaní paketu");
        return;
    }
}

void PacketAnalyzer::forwardPacket(const std::string &dstMac, IP ip)
{
    EthernetII eth =
            EthernetII(dstMac, port->hw_address()) /
            ip;
    try
    {
        sender->send(eth);
    }
    catch (invalid_interface &)
    {
        emit raise("Nastala chyba pri odosielaní paketu");
        return;
    }
}

void PacketAnalyzer::neighborForwardPacket(const string &dstMac, IP ip)
{
    EthernetII eth =
            EthernetII(HWAddress<6>(dstMac), neighMacAddr) /
            ip;
    try
    {
        neighborSender->send(eth);
    }
    catch (invalid_interface &)
    {
        emit raise("Nastala chyba pri odosielaní paketu");
        return;
    }
}

void PacketAnalyzer::enableRip()
{
    cout << portName << ": zapinam RIP" << endl;
    ripEnable = true;
}

void PacketAnalyzer::disableRip()
{
    cout << portName << ": vypinam RIP" << endl;
    ripEnable = false;
}

void PacketAnalyzer::setName(const string &name)
{
    this->portName = name;
    this->port = new NetworkInterface(name);
    this->sender = new PacketSender(*port);
}

void PacketAnalyzer::setIPAddr(const string &ipAddr)
{
    this->ipAddr = ipAddr;
}

void PacketAnalyzer::setMask(const string &mask)
{
    this->mask = mask;
}

string PacketAnalyzer::getIpAddr() const
{
    return ipAddr;
}

string PacketAnalyzer::getName() const
{
    return portName;
}

string PacketAnalyzer::getMask() const
{
    return mask;
}

string PacketAnalyzer::getNetwork() const
{
    if (!ipAddr.empty() && !mask.empty())
    {
        return (IPv4Address(ipAddr) & IPv4Address(mask)).to_string();
    }
    return string();
}

string PacketAnalyzer::getNeighName() const
{
    return neighName;
}


string PacketAnalyzer::getNeighIpAddr() const
{
    return neighAddr;
}

string PacketAnalyzer::getNeighMask() const
{
    return neighMask;
}

string PacketAnalyzer::getNeighNetwork() const
{
    if (!neighAddr.empty() && !neighMask.empty())
    {
        return (IPv4Address(neighAddr) & IPv4Address(neighMask)).to_string();
    }
    return string();
}

void PacketAnalyzer::setNeighName(const string &name)
{
    neighName = name;
    neighborPort = new NetworkInterface(name);
    neighborSender = new PacketSender(*neighborPort);
}

void PacketAnalyzer::setNeighIPAddr(const string &ipAddr)
{
    this->neighAddr = ipAddr;
}

void PacketAnalyzer::setNeighMask(const string &mask)
{
    this->neighMask = mask;
}
