#include "ripdatabase.h"
#include <iostream>
#include <QTimer>

#define INVALID_TIMER 40
#define FLUSH_TIMER 50
#define CHECK_INTERVAL 1200

using std::cout;
using std::endl;

RipDatabase::RipDatabase(QObject *parent) : QObject(parent)
{
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(checkRipEntries()));
    timer->start(CHECK_INTERVAL);

    invalidTimer = INVALID_TIMER;
    holddownTimer = INVALID_TIMER;
    flushTimer = FLUSH_TIMER;
}

void RipDatabase::addEntry(const RipDatabaseEntry &entry)
{
    RipDatabaseEntry newEntry = entry;
    if (newEntry.sourcePort != "connected")
    {
        newEntry.invalid.start();
        newEntry.flush.start();
    }

    database.push_back(newEntry);
    emit sendToRoutingTable(newEntry);
}

void RipDatabase::ripEntryReceived(const RipDatabaseEntry &entry)
{
    bool found = false;

    for (vector<RipDatabaseEntry>::iterator it = database.begin(); it != database.end(); ++it)
    {
        cout << endl;
        cout << "RIP_DATABASE: " << it->metric << endl;
        cout << "RIP_DATABASE: " << it->network << endl;
        cout << "RIP_DATABASE: " << it->mask << endl;
        cout << "RIP_DATABASE: " << it->nextHop << endl;
        cout << "RIP_DATABASE: " << it->sourcePort << endl;
        cout << endl;

        if ((it->network == entry.network) &&
            (it->mask == entry.mask))
        {
            found = true;

            if (entry.metric == 16)
            {
                if (it->metric != 16)
                    invalidateRoute(it);
            }
            else if (entry.metric <= it->metric)
            {
                if (entry.metric < it->metric)
                    it->metric = entry.metric;

                if (it->sourcePort != entry.sourcePort)
                    it->sourcePort = entry.sourcePort;

                it->invalid.restart();
                it->flush.restart();
            }
        }
    }

    if (!found)
        addEntry(entry);
}

vector<RipDatabaseEntry> RipDatabase::findRipRoutes(const QString &portName)
{
    vector<RipDatabaseEntry> res;
    string port = portName.toStdString();

    for (RipDatabaseEntry entry : database)
    {
        if (entry.sourcePort != port)
            res.push_back(entry);
    }
    return res;
}

void RipDatabase::invalidateRoute(vector<RipDatabaseEntry>::iterator it)
{
    it->holddown.start();
    it->metric = 16;

    if (it->sourcePort != "connected")
    {
        cout << "RIP_DATABASE: " << it->network << endl;
        QString route = QString::fromStdString(it->network);
        emit removeFromRoutingTable(route);
    }
}

void RipDatabase::checkRipEntries()
{
    for (vector<RipDatabaseEntry>::iterator it = database.begin(); it != database.end(); )
    {
        if (it->network.empty())
        {
            it = database.erase(it);
            continue;
        }

        if (it->sourcePort == "connected")
        {
            if (it->metric == 16)
                it->metric = 0;
            ++it;
            continue;
        }

        int invalid = it->invalid.elapsed() / 1e3;
        int flush = it->flush.elapsed() / 1e3;

        if (invalid > invalidTimer)
            invalidateRoute(it);

        if (flush > flushTimer)
        {
            it = database.erase(it);
        }
        else
        {
            ++it;
        }
    }
}

void RipDatabase::removeEntry(const string &network)
{
    for (vector<RipDatabaseEntry>::iterator it = database.begin(); it != database.end(); ++it)
    {
        if (it->network == network)
            it = database.erase(it);
    }
}

int RipDatabase::getHolddownTimer() const
{
    return holddownTimer;
}

int RipDatabase::getInvalidTimer() const
{
    return invalidTimer;
}

int RipDatabase::getFlushTimer() const
{
    return flushTimer;
}

void RipDatabase::setHolddownTimer(const int &timer)
{
    holddownTimer = timer;
}

void RipDatabase::setInvalidTimer(const int &timer)
{
    invalidTimer = timer;
}

void RipDatabase::setFlushTimer(const int &timer)
{
    flushTimer = timer;
}
