#ifndef __ARPENTRY_H__
#define __ARPENTRY_H__

#include <QTime>
#include <QMetaType>
#include <string>

using std::string;

struct ArpEntry
{
    string protAddr;
    string hwAddr;
    QTime age;

    inline void resetTimer()
    {
        age.restart();
    }

    inline uint32_t getAge()
    {
        return (age.elapsed() / 1e3);
    }
};

Q_DECLARE_METATYPE(ArpEntry)

#endif // __ARPENTRY_H__
