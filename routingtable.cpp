#include "routingtable.h"

using namespace Tins;
using std::string;
using std::cout;
using std::endl;

RoutingTable::RoutingTable(QObject *parent) : QObject(parent)
{

}

void RoutingTable::addRoute(const RoutingTableEntry &route)
{
    int ad = route.ad;
    cout << "RT: ZDROJ: " << ad << endl;
    cout << "RT: SIET: " << route.network << endl;
    cout << "RT: MASKA: " << route.mask << endl;
    cout << "RT: NEXT HOP: " << route.nextHop << endl;
    cout << "RT: METRIKA: " << route.metric << endl;

    for (vector<RoutingTableEntry>::iterator it = routes.begin(); it < routes.end(); )
    {
        // TODO: UPDATE GUI WHEN REPLACING ROUTES
        if (it->network == route.network &&
            it->mask == route.mask)
        {
            if (route.ad < it->ad)
            {
                it = routes.erase(it);
                routes.push_back(route);
                return;
            }
            else if (route.ad == it->ad && route.metric < it->metric)
            {
                it = routes.erase(it);
                routes.push_back(route);
                return;
            }

            return;
        }
        ++it;
    }

    routes.push_back(route);
    emit routeAdded(route);
}

RoutingTableEntry RoutingTable::lookupRoute(const QString &dstIP)
{
    string ipAddr = dstIP.toStdString();

    QString nextHop;
    string dstNetwork;
    RoutingTableEntry bestMatch;
    bestMatch.ad = 255;

    for (RoutingTableEntry entry : routes)
    {
        cout << "RT: CIELOVA IP ADRESA " << ipAddr << endl;
        try
        {
            dstNetwork = (IPv4Address(ipAddr) & IPv4Address(entry.mask)).to_string();
        }
        catch (Tins::invalid_address)
        {
            return bestMatch;
        }

        cout << endl;
        cout << "RT: CIELOVA SIET " << dstNetwork << endl;

        if (entry.network == dstNetwork)
        {
            cout << "RT: NASLA SA ZHODA " << endl;

            if (entry.mask > bestMatch.mask)
            {
                cout << "RT: MASKA " << entry.mask << " JE SPECIFICKEJSIA " << endl;
                bestMatch = entry;
            }
            else if (entry.mask == bestMatch.mask)
            {
                if (entry.ad < bestMatch.ad)
                {
                    int ad = entry.ad;
                    cout << "RT: AD " << ad << " JE LEPSIA " << endl;
                    bestMatch = entry;
                }
            }
        }
    }

    if (!bestMatch.outIface.empty())
    {
        cout << "RT: POSIELAM NA ROZHRANIE " << bestMatch.outIface << endl;
        cout << endl;
        return bestMatch;
    }

    nextHop = QString::fromStdString(bestMatch.nextHop);
    return lookupRoute(nextHop);
}

// TODO: Odstrániť cestu aj z GUI
void RoutingTable::removeEntry(const string &network)
{
    for (vector<RoutingTableEntry>::iterator it = routes.begin(); it < routes.end(); )
    {
        if (it->network == network)
        {
            it = routes.erase(it);
            return;
        }
        else
        {
            ++it;
        }
    }
}

bool RoutingTable::isKnownNetwork(const QString &dstIP)
{
    string dstNetwork;
    string ipAddr = dstIP.toStdString();

    for (RoutingTableEntry route : routes)
    {
        try
        {
            dstNetwork = (IPv4Address(ipAddr) & IPv4Address(route.mask)).to_string();
        }
        catch (Tins::invalid_address)
        {
            return false;
        }

        if (dstNetwork == route.network)
            return true;
    }
    return false;
}

void RoutingTable::receivedNewStaticRoute(const RoutingTableEntry &entry)
{
    addRoute(entry);
}

void RoutingTable::receivedRipDatabaseEntry(const RipDatabaseEntry &entry)
{
    RoutingTableEntry route;
    route.ad = Constants::RIP_AD;
    route.metric = entry.metric;
    route.network = entry.network;
    route.mask = entry.mask;
    route.nextHop = entry.nextHop;
    route.outIface = entry.sourcePort;
    addRoute(route);
}
