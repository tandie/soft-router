#include "interface.h"
#include <tins/network_interface.h>
#include <tins/exceptions.h>

using namespace Tins;
using std::string;
using std::vector;

Interface::Interface()
{
    try
    {
        NetworkInterface::default_interface();
    }
    catch (invalid_interface &)
    {

    }
}

vector<string> Interface::getAllInterfaces()
{
    vector<string> result;
    for (const NetworkInterface &iface : NetworkInterface::all()) {
        if (!iface.is_loopback() &&
             iface.name() != "wlp2s0" &&
             iface.name() != "docker0" &&
             iface.name().find("br") == std::string::npos)
        {
            result.push_back(iface.name());
        }
    }
    return result;
}
