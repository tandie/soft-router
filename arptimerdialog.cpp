#include "arptimerdialog.h"
#include "ui_arptimerdialog.h"

ArpTimerDialog::ArpTimerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ArpTimerDialog)
{
    ui->setupUi(this);
}

ArpTimerDialog::~ArpTimerDialog()
{
    delete ui;
}

void ArpTimerDialog::setValues(uint32_t time)
{
    ui->leArpTimer->setText(QString::number(time));
}

void ArpTimerDialog::on_btnSetArpTimer_clicked()
{
    emit arpTimerChanged(ui->leArpTimer->text());
    close();
}
