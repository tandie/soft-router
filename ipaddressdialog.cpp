#include "ipaddressdialog.h"
#include "ui_ipaddressdialog.h"

IPAddressDialog::IPAddressDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IPAddressDialog)
{
    ui->setupUi(this);
}

IPAddressDialog::~IPAddressDialog()
{
    delete ui;
}

void IPAddressDialog::setValues(int pos, QString ipAddr, QString mask)
{
    if (pos == 0)
    {
        ui->port1IP->setText(ipAddr);
        ui->port1Mask->setText(mask);
    }
    else
    {
        ui->port2IP->setText(ipAddr);
        ui->port2Mask->setText(mask);
    }
}

void IPAddressDialog::on_setIPParams_clicked()
{
    emit ipParamsChanged(0, ui->port1IP->text(), ui->port1Mask->text());
    emit ipParamsChanged(1, ui->port2IP->text(), ui->port2Mask->text());
    close();
}
