TEMPLATE = app
TARGET = soft-router

QT = core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

SOURCES += main.cpp mainwindow.cpp \
    interface.cpp \
    packetanalyzer.cpp \
    routingtable.cpp \
    arptable.cpp \
    staticroutedialog.cpp \
    arptimerdialog.cpp \
    ipaddressdialog.cpp \
    riptimerdialog.cpp \
    ripdatabase.cpp

HEADERS += exceptions.h mainwindow.h \
    packetinfo.h \
    arpentry.h \
    interface.h \
    packetanalyzer.h \
    routingtableentry.h \
    routingtable.h \
    arptable.h \
    staticroutedialog.h \
    arptimerdialog.h \
    ipaddressdialog.h \
    riptimerdialog.h \
    ripdatabase.h \
    ripdatabaseentry.h \
    constants.h

LIBS += -ltins

FORMS += mainwindow.ui \
    staticroutedialog.ui \
    arptimerdialog.ui \
    ipaddressdialog.ui \
    riptimerdialog.ui
