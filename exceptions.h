#ifndef __EXCEPTIONS_H__
#define __EXCEPTIONS_H__

#include <stdexcept>

namespace SoftRouter
{
    class InterfaceDown : public std::runtime_error
    {
        using std::runtime_error::runtime_error;
    };
} // SoftRouter

#endif // __EXCEPTIONS_H__
