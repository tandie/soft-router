#ifndef ARPTIMERDIALOG_H
#define ARPTIMERDIALOG_H

#include <QDialog>

namespace Ui {
class ArpTimerDialog;
}

class ArpTimerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ArpTimerDialog(QWidget *parent = 0);
    ~ArpTimerDialog();
    void setValues(uint32_t time);

signals:
    void arpTimerChanged(QString);

private slots:
    void on_btnSetArpTimer_clicked();

private:
    Ui::ArpTimerDialog *ui;
};

#endif // ARPTIMERDIALOG_H
