#ifndef __PACKETANALYZER_H__
#define __PACKETANALYZER_H__

#include <QThread>
#include <QTimer>
#include <QString>
#include <atomic>
#include <string>
#include <vector>
#include <functional>
#include <iostream>
#include <tins/tins.h>

#include "exceptions.h"
#include "packetinfo.h"
#include "arpentry.h"
#include "routingtableentry.h"
#include "ripdatabaseentry.h"
#include "constants.h"

#define MAC_BYTES 6

using namespace Tins;
using std::vector;
using std::atomic_bool;
using std::string;

class PacketAnalyzer : public QObject
{
    Q_OBJECT

public:
    PacketAnalyzer();
    void startSniff(const string &name);
    void abortSniffing();

    void setName(const string &name);
    void setIPAddr(const string &ipAddr);
    void setMask(const string &mask);

    void setNeighName(const string &name);
    void setNeighIPAddr(const string &ipAddr);
    void setNeighMask(const string &mask);

    string getIpAddr() const;
    string getMask() const;
    string getNetwork() const;

    string getNeighIpAddr() const;
    string getNeighMask() const;
    string getNeighNetwork() const;

    string getName() const;
    string getNeighName() const;

    void enableRip();
    void disableRip();
    void sendRip();

signals:
    void raise(const char*);
    void arpPacketReceived(const PacketInfo&);
    void sendToRipDatabase(const RipDatabaseEntry&);
    bool isKnownNetwork(const QString&);

    RoutingTableEntry lookupRoute(const QString&);
    ArpEntry findDstMac(const QString&);
    vector<RipDatabaseEntry> getRipRoutes(const QString&);

private:
    bool handle(const Packet &packet);
    void handleARP(const Packet &packet, PacketInfo &pktInfo);
    void handleIP(const Packet &packet, PacketInfo &pktInfo);
    void handleICMP(const Packet &packet, PacketInfo &pktInfo);
    void handleRIP(const Packet &packet, PacketInfo &pktInfo);
    void ARPRequest(PacketInfo &pktInfo);
    void neighborARPRequest(PacketInfo &pktInfo);
    void forwardPacket(const string &dstMac, IP ip);
    void neighborForwardPacket(const string &dstMac, IP ip);

    QThread worker;
    atomic_bool abort;
    atomic_bool ripEnable;

    PacketSender *sender;
    NetworkInterface *port;

    PacketSender *neighborSender;
    NetworkInterface *neighborPort;

    string ipAddr;
    string mask;
    string portName;
    HWAddress<MAC_BYTES> macAddr;

    string neighAddr;
    string neighMask;
    string neighName;
    HWAddress<MAC_BYTES> neighMacAddr;

private slots:
    void run();

};

#endif // __PACKETANALYZER_H__
