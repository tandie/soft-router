#ifndef ROUTINGTABLE_H
#define ROUTINGTABLE_H

#include <QObject>
#include <vector>
#include <string>
#include <tins/ip_address.h>
#include <tins/exceptions.h>
#include <iostream>
#include "routingtableentry.h"
#include "ripdatabaseentry.h"
#include "packetinfo.h"
#include "constants.h"

using std::string;
using std::vector;

class RoutingTable : public QObject
{
    Q_OBJECT

public:
    explicit RoutingTable(QObject *parent = nullptr);
    void addRoute(const RoutingTableEntry &route);
    void addEntry(const RoutingTableEntry &route);
    void removeEntry(const std::string &network);

public slots:
    RoutingTableEntry lookupRoute(const QString &dstIP);
    bool isKnownNetwork(const QString &dstIP);
    void receivedRipDatabaseEntry(const RipDatabaseEntry &entry);
    void receivedNewStaticRoute(const RoutingTableEntry &entry);

signals:
    void routeAdded(const RoutingTableEntry &entry);

private:
    vector<RoutingTableEntry> routes;
};

#endif // ROUTINGTABLE_H
