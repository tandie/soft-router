#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "interface.h"
#include "packetinfo.h"
#include "exceptions.h"
#include <QMessageBox>
#include <QCloseEvent>
#include <QTableWidgetItem>
#include <QTimer>
#include <iostream>

#define RIP_TIMER 30
#define ARP_TIMER 3600
#define CHECK_INTERVAL 1000

using SoftRouter::InterfaceDown;
using std::string;
using std::cout;
using std::endl;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sniffOnRun = false;
    arpTable.setTimer(ARP_TIMER);
    updateTimer = RIP_TIMER;

    QTimer *arpTime = new QTimer(this);
    connect(arpTime, SIGNAL(timeout()), this, SLOT(updateArpEntryTimer()));
    arpTime->start(CHECK_INTERVAL);

    connect(&routingTable, SIGNAL(routeAdded(RoutingTableEntry)),
            this, SLOT(showRoute(RoutingTableEntry)));

    connect(&ipAddressDialog, SIGNAL(ipParamsChanged(int,QString,QString)),
            this, SLOT(updateIPParams(int,QString,QString)));

    connect(this, SIGNAL(clearArpEntries()), &arpTable, SLOT(clear()));

    connect(&arpTable, SIGNAL(arpEntryRemoved(int)), this, SLOT(hideArpEntry(int)));

    connect(&arpTable, SIGNAL(arpEntryAdded(ArpEntry)), this, SLOT(showArpEntry(ArpEntry)));

    connect(&arpTable, SIGNAL(arpEntryReset(int)), this, SLOT(resetArpEntry(int)));

    connect(&arpTable, SIGNAL(arpEntryUpdated(int,PacketInfo)),
            this, SLOT(updateArpEntry(int,PacketInfo)));

    connect(&ripDatabase, SIGNAL(sendToRoutingTable(RipDatabaseEntry)),
            &routingTable, SLOT(receivedRipDatabaseEntry(RipDatabaseEntry)));

    connect(&staticRouteDialog, SIGNAL(staticRouteAdded(RoutingTableEntry)),
            &routingTable, SLOT(receivedNewStaticRoute(RoutingTableEntry)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::printAllInterfaces()
{
    uint8_t i = 0;
    string ipAddr;
    string network;
    string mask;
    string nextHop;
    RipDatabaseEntry entry;

    QString netMask;
    QString portName;
    QString portIpAddr;

    for (const string &name : Interface::getAllInterfaces())
    {
        if (i >= 2)
            break;

        if (i == 0)
        {
            ipAddr = "192.168.0.1";
            mask = "255.255.255.252";

            netMask = QString::fromStdString(mask);
            portIpAddr = QString::fromStdString(ipAddr);
            portName = QString::fromStdString(name);

            ui->p1Name->setText(portName);
            ui->p1IPAddr->setText(portIpAddr);
            ui->p1Mask->setText(netMask);

            sniffers[i+1].setNeighIPAddr(ipAddr);
            sniffers[i+1].setNeighMask(mask);
            sniffers[i+1].setNeighName(name);
        }
        else if (i == 1)
        {
            ipAddr = "172.16.0.1";
            mask = "255.255.255.0";

            netMask = QString::fromStdString(mask);
            portIpAddr = QString::fromStdString(ipAddr);
            portName = QString::fromStdString(name);

            ui->p2Name->setText(portName);
            ui->p2IPAddr->setText(portIpAddr);
            ui->p2Mask->setText(netMask);

            sniffers[i-1].setNeighIPAddr(ipAddr);
            sniffers[i-1].setNeighMask(mask);
            sniffers[i-1].setNeighName(name);
        }

        try
        {
            sniffers[i].startSniff(name);
        }
        catch (InterfaceDown &e)
        {
            QMessageBox::critical(this, windowTitle(), QString::fromUtf8(e.what()));
            return;
        }

        sniffers[i].setIPAddr(ipAddr);
        sniffers[i].setMask(mask);
        network = (IPv4Address(ipAddr) & IPv4Address(mask)).to_string();

        RoutingTableEntry route;
        route.ad = 0;
        route.network = network;
        route.mask = mask;
        route.nextHop = nextHop;
        route.metric = 0;
        route.outIface = name;

        routingTable.addRoute(route);

        entry.metric = 0;
        entry.network = network;
        entry.mask = mask;
        entry.nextHop = string();
        entry.sourcePort = "connected";

        cout << "MAIN WINDOW: " << entry.network << endl;
        cout << "MAIN WINDOW: " << entry.mask << endl;
        cout << "MAIN WINDOW: " << entry.nextHop << endl;
        cout << "MAIN WINDOW: " << entry.metric << endl;

        ripDatabase.addEntry(entry);
        i++;
    }
}

QString MainWindow::getPort(int port) const
{
    if (port != -1)
        return QString::number(port);
    else
        return QString();
}

void MainWindow::snifferAbort()
{
    sniffOnRun = false;
    sniffers[0].abortSniffing();
    sniffers[1].abortSniffing();
}

void MainWindow::snifferRun()
{
    for (int i = 0; i < 2; i++)
    {
        connect(&sniffers[i], SIGNAL(raise(const char*)),
                this, SLOT(handleError(const char*)));

        connect(&sniffers[i], SIGNAL(arpPacketReceived(PacketInfo)),
                &arpTable, SLOT(processArpPacket(PacketInfo)));

        connect(&sniffers[i], SIGNAL(findDstMac(QString)),
                &arpTable, SLOT(findDstMac(QString)),
                Qt::BlockingQueuedConnection);

        connect(&sniffers[i], SIGNAL(lookupRoute(QString)),
                &routingTable, SLOT(lookupRoute(QString)),
                Qt::BlockingQueuedConnection);

        connect(&sniffers[i], SIGNAL(sendToRipDatabase(RipDatabaseEntry)),
                &ripDatabase, SLOT(ripEntryReceived(RipDatabaseEntry)));

        connect(&sniffers[i], SIGNAL(getRipRoutes(QString)),
                &ripDatabase, SLOT(findRipRoutes(QString)));

        connect(&sniffers[i], SIGNAL(isKnownNetwork(QString)),
                &routingTable, SLOT(isKnownNetwork(QString)),
                Qt::BlockingQueuedConnection);
    }

    QTimer *ripTimer = new QTimer(this);
    connect(ripTimer, SIGNAL(timeout()), this, SLOT(sendRIPResponse()));
    ripTimer->start(updateTimer * 1000);
}

void MainWindow::sendRIPResponse()
{
    sniffers[0].sendRip();
    sniffers[1].sendRip();
}

void MainWindow::showEvent(QShowEvent *event)
{
    initialiseTable();
    printAllInterfaces();
    snifferRun();
    QMainWindow::showEvent(event);
}

void MainWindow::handleError(const char *err_msg)
{
    QMessageBox::critical(this, windowTitle(), QString::fromUtf8(err_msg));
    this->snifferAbort();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    event->accept();
}

void MainWindow::initialiseTable()
{
    int available = ui->routingTable->width();
    ui->routingTable->setColumnWidth(0, available/14);
    ui->routingTable->setColumnWidth(1, available/14);
    ui->routingTable->setColumnWidth(2, 3*available/14);
    ui->routingTable->setColumnWidth(3, 3*available/14);
    ui->routingTable->setColumnWidth(4, 3*available/14);
    ui->routingTable->setColumnWidth(5, 3*available/16);

    available = ui->arpTable->width();
    ui->arpTable->setColumnWidth(0, 3*available/8);
    ui->arpTable->setColumnWidth(1, 3*available/8);
    ui->arpTable->setColumnWidth(2, available/4.9);
}

void MainWindow::showArpEntry(const ArpEntry &entry)
{
    int row = ui->arpTable->rowCount();

    ui->arpTable->insertRow(row);

    QString protAddr = QString::fromStdString(entry.protAddr);
    QString hwAddr = QString::fromStdString(entry.hwAddr);

    ui->arpTable->setItem(row, 0, new QTableWidgetItem( protAddr ) );
    ui->arpTable->setItem(row, 1, new QTableWidgetItem( hwAddr ) );
    ui->arpTable->setItem(row, 2, new QTableWidgetItem( "0" ));
}

void MainWindow::updateArpEntryTimer()
{
    int newAge;
    int numOfRows = ui->arpTable->rowCount();
    QString currentAge;

    for (int row = 0; row < numOfRows; row++)
    {
        currentAge = ui->arpTable->item(row, 2)->text();
        newAge = currentAge.toInt() + 1;
        ui->arpTable->setItem( row, 2, new QTableWidgetItem(QString::number(newAge)) );
    }
}

void MainWindow::updateArpEntry(int pos, const PacketInfo &pktInfo)
{
    QString hwAddr = QString::fromStdString(pktInfo.srcHwAdd);
    ui->arpTable->setItem(pos, 0, new QTableWidgetItem( hwAddr ));
    ui->arpTable->setItem(pos, 2, new QTableWidgetItem( "0" ));
}

void MainWindow::resetArpEntry(int pos)
{
    ui->arpTable->setItem(pos, 2, new QTableWidgetItem( "0" ));
}

void MainWindow::on_actionStatick_cestu_triggered()
{
    staticRouteDialog.setModal(true);
    staticRouteDialog.exec();
}

void MainWindow::on_actionARP_asova_triggered()
{
    arpTimerDialog.setModal(true);
    arpTimerDialog.setValues(arpTable.getTimer());
    arpTimerDialog.exec();
}

void MainWindow::on_actionIP_adresy_triggered()
{
    ipAddressDialog.setModal(true);
    ipAddressDialog.setValues(0, ui->p1IPAddr->text(), ui->p1Mask->text());
    ipAddressDialog.setValues(1, ui->p2IPAddr->text(), ui->p2Mask->text());
    ipAddressDialog.exec();
}

void MainWindow::on_actionRIP_asova_e_triggered()
{
    ripTimerDialog.setModal(true);
    ripTimerDialog.exec();
}

void MainWindow::on_btnClearArp_clicked()
{
    emit clearArpEntries();
    ui->arpTable->setRowCount(0);
}

void MainWindow::on_p2RipEnable_stateChanged(int arg1)
{
    toggleRip(arg1, 1);
}

void MainWindow::on_p1RipEnable_stateChanged(int arg1)
{
    toggleRip(arg1, 0);
}

void MainWindow::toggleRip(int arg1, int port)
{
    if (arg1 == 0)
        sniffers[port].disableRip();
    else if (arg1 == 2)
        sniffers[port].enableRip();
}

void MainWindow::showRoute(const RoutingTableEntry &entry)
{
    uint8_t idx = ui->routingTable->rowCount();

    QString source;
    QString metric = QString::number(entry.metric);
    QString network = QString::fromStdString(entry.network);
    QString mask = QString::fromStdString(entry.mask);
    QString nextHop = QString::fromStdString(entry.nextHop);
    QString outIface = QString::fromStdString(entry.outIface);

    ui->routingTable->insertRow(idx);

    switch(entry.ad)
    {
        case 0: source = "C"; break;
        case 1: source = "S"; break;
        case 120: source = "R"; break;
        default: source = "U";
    }

    ui->routingTable->setItem(idx, 0, new QTableWidgetItem(source));
    ui->routingTable->setItem(idx, 1, new QTableWidgetItem(metric));
    ui->routingTable->setItem(idx, 2, new QTableWidgetItem(network));
    ui->routingTable->setItem(idx, 3, new QTableWidgetItem(mask));
    ui->routingTable->setItem(idx, 4, new QTableWidgetItem(nextHop));
    ui->routingTable->setItem(idx, 5, new QTableWidgetItem(outIface));
}

void MainWindow::hideArpEntry(int pos)
{
    ui->arpTable->removeRow(pos);
}

void MainWindow::updateIPParams(int port, QString ipAddr, QString mask)
{
    if (port != 0 && port != 1)
        return;

    string oldNetwork;
    IPv4Address netAddr;
    IPv4Address netMask;
    QString network;

    cout << "MAIN WINDOW: NOVA IP ADRESA " << ipAddr.toStdString();
    cout << "MAIN WINDOW: POVODNA IP ADRESA " << sniffers[port].getIpAddr();

    if (ipAddr.toStdString() != sniffers[port].getIpAddr())
    {
        oldNetwork = sniffers[port].getNetwork();
        ripDatabase.removeEntry( oldNetwork );

        netMask = IPv4Address(mask.toStdString());
        netAddr = IPv4Address(ipAddr.toStdString()) & netMask;

        RipDatabaseEntry entry;
        entry.metric = 0;
        entry.network = netAddr.to_string();
        entry.mask = mask.toStdString();
        entry.nextHop = string();
        entry.sourcePort = "connected";

        cout << "MAIN WINDOW: Adresa novej siete " << entry.network << endl;
        cout << "MAIN WINDOW: Maska novej siete " << entry.mask << endl;
        cout << "MAIN WINDOW: Metrika novej siete " << entry.metric << endl;

        ripDatabase.addEntry(entry);

        sniffers[port].setIPAddr(ipAddr.toStdString());
        sniffers[port].setMask(mask.toStdString());

        if (port == 0)
        {
            ui->p1IPAddr->setText(ipAddr);
            ui->p1Mask->setText(mask);
        }
        else if (port == 1)
        {
            ui->p2IPAddr->setText(ipAddr);
            ui->p2Mask->setText(mask);
        }

        network = QString::fromStdString(netAddr.to_string());

        ui->routingTable->setItem(port, 2, new QTableWidgetItem(network));
        ui->routingTable->setItem(port, 3, new QTableWidgetItem(mask));
    }
}

void MainWindow::updateArpTimer(QString time)
{
    QString msg = "ARP časovač nastavený na " + time + " s";
    arpTable.setTimer(time.toInt());
    QMessageBox::information(this, windowTitle(), msg);
}

void MainWindow::hideRoute(int pos)
{
    ui->routingTable->removeRow(pos);
}

void MainWindow::on_action_tart_triggered()
{
    this->snifferRun();
}

void MainWindow::on_actionStop_triggered()
{
    this->snifferAbort();
}
