#ifndef RIPDATABASE_H
#define RIPDATABASE_H

#include <QObject>
#include <QTime>
#include <vector>
#include "ripdatabaseentry.h"
#include "packetinfo.h"
#include "constants.h"

using std::string;
using std::vector;

class RipDatabase : public QObject
{
    Q_OBJECT

public:
    explicit RipDatabase(QObject *parent = nullptr);
    void addEntry(const RipDatabaseEntry &entry);
    void removeEntry(const string &network);
    int getHolddownTimer() const;
    int getInvalidTimer() const;
    int getFlushTimer() const;
    void setHolddownTimer(const int &timer);
    void setInvalidTimer(const int &timer);
    void setFlushTimer(const int &timer);

signals:
    void sendToRoutingTable(const RipDatabaseEntry&);
    void removeFromRoutingTable(const QString&);

public slots:
    void ripEntryReceived(const RipDatabaseEntry &entry);
    vector<RipDatabaseEntry> findRipRoutes(const QString &portName);

private:
    vector<RipDatabaseEntry> database;
    QTime updateTimer;
    int holddownTimer;
    int flushTimer;
    int invalidTimer;

    void invalidateRoute(std::vector<RipDatabaseEntry>::iterator it);

private slots:
    void checkRipEntries();

};

#endif // RIPDATABASE_H
