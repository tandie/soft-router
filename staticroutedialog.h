#ifndef STATICROUTEDIALOG_H
#define STATICROUTEDIALOG_H

#include <QDialog>
#include "routingtableentry.h"

namespace Ui {
class StaticRouteDialog;
}

class StaticRouteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StaticRouteDialog(QWidget *parent = 0);
    ~StaticRouteDialog();

signals:
    void staticRouteAdded(const RoutingTableEntry &entry);

private slots:
    void on_btnAddStaticRoute_clicked();

private:
    Ui::StaticRouteDialog *ui;
};

#endif // STATICROUTEDIALOG_H
