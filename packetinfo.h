#ifndef __PACKETINFO_H__
#define __PACKETINFO_H__

#include <string>
#include <vector>
#include <QMetaType>

using std::string;
using std::vector;

struct PacketInfo
{
    using PortType = int;
    string srcProtAdd;
    string dstProtAdd;
    string srcHwAdd;
    string dstHwAdd;
    PortType dport = -1;
    PortType sport = -1;
};

Q_DECLARE_METATYPE(PacketInfo)

#endif // __PACKETINFO_H__
