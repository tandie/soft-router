#include "staticroutedialog.h"
#include "ui_staticroutedialog.h"

StaticRouteDialog::StaticRouteDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StaticRouteDialog)
{
    ui->setupUi(this);
}

StaticRouteDialog::~StaticRouteDialog()
{
    delete ui;
}

void StaticRouteDialog::on_btnAddStaticRoute_clicked()
{
    RoutingTableEntry entry;
    entry.metric = 0;
    entry.ad = 1;
    entry.network = ui->leNetAddress->text().toStdString();
    entry.mask = ui->leMask->text().toStdString();
    entry.nextHop = ui->leNextHop->text().toStdString();
    entry.outIface = ui->leOutIface->text().toStdString();
    emit staticRouteAdded(entry);
    close();
}
