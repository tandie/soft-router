#ifndef __ARPTABLE_H__
#define __ARPTABLE_H__

#include <QObject>
#include <vector>
#include <QTimer>
#include <string>
#include <iostream>
#include "arpentry.h"
#include "packetinfo.h"

using std::vector;
using std::string;

class ArpTable : public QObject
{
    Q_OBJECT

public:
    ArpTable();
    ~ArpTable();
    void setTimer(const uint32_t time);
    uint32_t getTimer();

public slots:
    void clear();
    void processArpPacket(const PacketInfo &pktInfo);
    ArpEntry findDstMac(const QString &protAddr);

signals:
    void arpEntryRemoved(int);
    void arpEntryAdded(const ArpEntry&);
    void arpEntryUpdated(int, const PacketInfo&);
    void arpEntryReset(int);

private:
    vector<ArpEntry> arpTable;
    uint32_t invalidTimer;

    ArpEntry addEntry(const PacketInfo &pktInfo);

private slots:
    void removeOldEntries();

};

#endif // __ARPTABLE_H__
