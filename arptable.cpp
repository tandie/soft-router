#include "arptable.h"

#define REFRESH_TIME 1000
#define INVALID_TIMER 3600

using std::vector;
using std::endl;
using std::cout;

ArpTable::ArpTable()
{
    invalidTimer = INVALID_TIMER;
    QTimer *arpTimer = new QTimer(this);
    arpTimer->connect(arpTimer, SIGNAL(timeout()), this, SLOT(removeOldEntries()));
    arpTimer->start(REFRESH_TIME);
}

ArpTable::~ArpTable()
{

}

void ArpTable::processArpPacket(const PacketInfo &pktInfo)
{
    int i = 0;
    for (ArpEntry entry : arpTable)
    {
        if (entry.protAddr == pktInfo.srcProtAdd)
        {
            if (entry.hwAddr == pktInfo.srcHwAdd)
            {
                entry.resetTimer();
                emit arpEntryReset(i);
                return;
            }
            else
            {
                entry.hwAddr = pktInfo.srcHwAdd;
                arpEntryUpdated(i, pktInfo);
                return;
            }
        }
        else if (entry.hwAddr == pktInfo.srcHwAdd)
        {
            if (entry.protAddr == pktInfo.srcProtAdd)
            {
                entry.resetTimer();
                emit arpEntryReset(i);
                return;
            }
        }
        i++;
    }
    ArpEntry entry = addEntry(pktInfo);
    emit arpEntryAdded(entry);
}

void ArpTable::removeOldEntries()
{
    int i = 0;
    for (vector<ArpEntry>::iterator it = arpTable.begin(); it < arpTable.end(); )
    {
        if (it->getAge() >= invalidTimer)
        {
            emit arpEntryRemoved(i);
            it = arpTable.erase(it);
        }
        else
        {
            it++;
        }
        i++;
    }
}

ArpEntry ArpTable::findDstMac(const QString &protAddr)
{
    string ipAddr = protAddr.toStdString();
    ArpEntry emptyEntry;
    emptyEntry.hwAddr = string();

    for (ArpEntry entry : arpTable)
    {
        if (entry.protAddr == ipAddr)
            return entry;
    }

    return emptyEntry;
}

ArpEntry ArpTable::addEntry(const PacketInfo &pktInfo)
{
    ArpEntry entry;
    entry.hwAddr = pktInfo.srcHwAdd;
    entry.protAddr = pktInfo.srcProtAdd;
    entry.age.start();
    arpTable.push_back(entry);
    return entry;
}

void ArpTable::clear()
{
    arpTable.clear();
}

void ArpTable::setTimer(const uint32_t time)
{
    invalidTimer = time;
}

uint32_t ArpTable::getTimer()
{
    return invalidTimer;
}
