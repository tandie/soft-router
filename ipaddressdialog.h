#ifndef IPADDRESSDIALOG_H
#define IPADDRESSDIALOG_H

#include <QDialog>

namespace Ui {
class IPAddressDialog;
}

class IPAddressDialog : public QDialog
{
    Q_OBJECT

public:
    explicit IPAddressDialog(QWidget *parent = 0);
    ~IPAddressDialog();
    void setValues(int pos, QString ipAddr, QString mask);

signals:
    void ipParamsChanged(int, QString, QString);

private slots:
    void on_setIPParams_clicked();

private:
    Ui::IPAddressDialog *ui;
};

#endif // IPADDRESSDIALOG_H
