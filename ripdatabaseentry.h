#ifndef RIPDATABASEENTRY_H
#define RIPDATABASEENTRY_H

#include <string>
#include <QMetaType>
#include <QTime>

using std::string;

struct RipDatabaseEntry
{
    int metric = 0;
    string network = string();
    string mask = string();
    string nextHop = string();
    string sourcePort = string();
    QTime invalid;
    QTime holddown;
    QTime flush;
    bool IsInRoutingTable = false;
};

Q_DECLARE_METATYPE(RipDatabaseEntry)

#endif // RIPDATABASEENTRY_H
