#ifndef RIPTIMERDIALOG_H
#define RIPTIMERDIALOG_H

#include <QDialog>

namespace Ui {
class RipTimerDialog;
}

class RipTimerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RipTimerDialog(QWidget *parent = 0);
    ~RipTimerDialog();

private slots:
    void on_btnRipTimers_clicked();

private:
    Ui::RipTimerDialog *ui;
};

#endif // RIPTIMERDIALOG_H
