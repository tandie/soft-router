#include <QApplication>
#include <string>
#include "mainwindow.h"
#include "packetinfo.h"

int main(int argc, char **argv)
{ 
    qRegisterMetaType<PacketInfo>();
    qRegisterMetaType<ArpEntry>();
    qRegisterMetaType<RoutingTableEntry>();
    qRegisterMetaType<RipDatabaseEntry>();

    QApplication app(argc, argv);
    MainWindow window;
    window.show();

    return app.exec();
}
