#ifndef INTERFACE_H
#define INTERFACE_H

#include <QObject>
#include <string>
#include <vector>

using std::vector;
using std::string;

class Interface : public QObject
{
    Q_OBJECT

public:
    Interface();
    static vector<string> getAllInterfaces();
};

#endif // INTERFACE_H
